import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { GetDimensionsComponent } from './game/get-dimensions/get-dimensions.component';
import { GameContainerComponent } from './game/game-container/game-container.component';
import { GameBoardComponent } from './game/game-board/game-board.component';
import { GameCellComponent } from './game/game-cell/game-cell.component';
import { FieldErrorComponent } from './game/field-error/field-error.component';

@NgModule({
  declarations: [
    AppComponent,
    GetDimensionsComponent,
    GameContainerComponent,
    GameBoardComponent,
    GameCellComponent,
    FieldErrorComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
