import { Component, OnInit } from '@angular/core';
import { GameBoardService } from '../services/game-board.service';

@Component({
  selector: 'app-game-container',
  templateUrl: './game-container.component.html',
  styleUrls: ['./game-container.component.scss']
})
export class GameContainerComponent implements OnInit {

  constructor(private gameBoardService: GameBoardService) { }

  ngOnInit() {
  }

}
