import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameContainerComponent } from './game-container.component';
import { MockComponent } from 'ng2-mock-component';

describe('GameContainerComponent', () => {
    let component: GameContainerComponent;
    let fixture: ComponentFixture<GameContainerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GameContainerComponent,
                MockComponent({ selector: 'app-game-board' }),
                MockComponent({ selector: 'app-get-dimensions' })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GameContainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
