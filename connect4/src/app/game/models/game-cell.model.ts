export class GameCellModel {
    cellType = CellType.empty;
    column: number;
    row: number;
    cellClass: string;

    constructor(column: number, row: number) {
        this.column = column;
        this.row = row;
    }
}
export enum CellType {
    empty = 0,
    yellow,
    red
}
