import { GameBoardDimensionsModel } from './game-board-dimensions.model';
import { GameCellModel, CellType } from './game-cell.model';

export class GameBoardModel {
    askDimensions = true;
    gameBoardDimensions: GameBoardDimensionsModel = new GameBoardDimensionsModel();
    gameDataModel = new GameDataModel();
    tokenColor: CellType = CellType.red;
    hasGameFinished = false;
    isADraw = false;
}

export class GameDataModel {
    gameData: Array<Array<GameCellModel>> = new Array<Array<GameCellModel>>();
}
