export enum SearchDirectionType {
    north,
    northEast,
    East,
    southEast,
    south,
    southWest,
    West,
    northWest
}
