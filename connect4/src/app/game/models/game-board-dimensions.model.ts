export class GameBoardDimensionsModel {
    columns: number;
    rows: number;

    constructor() {
        this.columns = 0;
        this.rows = 0;
    }
}
