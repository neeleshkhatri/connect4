import { Injectable } from '@angular/core';
import { GameCellModel } from '../models/game-cell.model';
import * as _ from 'underscore';
import { SearchDirectionType } from '../models/game-search-direction.model';
import { GameDataModel } from '../models/game-board.model';

@Injectable({
  providedIn: 'root'
})
export class GameBoardFindWinnerService {
  connect4 = 4;
  constructor() { }

  findWinner(dropCell: GameCellModel, gameDataModel: GameDataModel): boolean {
    const directionTallyList = this.getDirectionTallyList();
    for (const direction in SearchDirectionType) {
      if (Number(direction)) {
        const sameTokenCount = this.getSameTokenCounts(dropCell, gameDataModel, Number(direction), 1);
        directionTallyList[Number(direction)] = sameTokenCount;
        if (sameTokenCount >= this.connect4) {
          return true;
        }
      }
    }
    if (this.checkIfWinner(directionTallyList)) {
      return true;
    }
    return false;
  }
  private checkIfWinner(directionTallyList: number[]): boolean {
    if (directionTallyList[SearchDirectionType.north] + directionTallyList[SearchDirectionType.south] > this.connect4) {
      return true;
    }
    if (directionTallyList[SearchDirectionType.northEast] + directionTallyList[SearchDirectionType.southWest] > this.connect4) {
      return true;
    }
    if (directionTallyList[SearchDirectionType.East] + directionTallyList[SearchDirectionType.West] > this.connect4) {
      return true;
    }
    if (directionTallyList[SearchDirectionType.southEast] + directionTallyList[SearchDirectionType.northWest] > this.connect4) {
      return true;
    }

    return false;
  }
  private getDirectionTallyList(): number[] {
    const tallyList = [];
    for (const direction in SearchDirectionType) {
        if ( Number(direction)) {
          tallyList.push(0);
        }
    }
    return tallyList;
  }
  private getRow(rowNum: number, gameData: Array<Array<GameCellModel>>): Array<GameCellModel> {
    if (rowNum >= 0 && rowNum < gameData.length) {
      return gameData[gameData.length - 1 - rowNum];
    }
    return undefined;
  }
  private getSameTokenCounts(startCell: GameCellModel,
    gameDataModel: GameDataModel, direction: SearchDirectionType, counter: number): number {

    const nextCell = this.getNextCell(startCell, gameDataModel, direction);
    if (nextCell) {
      if (nextCell.cellType === startCell.cellType) {
        return this.getSameTokenCounts(nextCell, gameDataModel, direction, counter + 1);
      } else {
        return counter;
      }
    } else {
      return counter;
    }
  }
  private getNextCell(startCell: GameCellModel, gameDataModel: GameDataModel, direction: SearchDirectionType): GameCellModel {
    let rowNum = startCell.row;
    let columnNum = startCell.column;
    const maxRowNum = gameDataModel.gameData.length - 1;
    const maxColumnNum = _.first(gameDataModel.gameData).length - 1;
    switch (direction) {
      case SearchDirectionType.north:
        rowNum++;
        break;
      case SearchDirectionType.northEast:
        rowNum++;
        columnNum++;
        break;
      case SearchDirectionType.East:
        columnNum++;
        break;
      case SearchDirectionType.southEast:
        rowNum--;
        columnNum++;
        break;
      case SearchDirectionType.south:
        rowNum--;
        break;
      case SearchDirectionType.southWest:
        rowNum--;
        columnNum--;
        break;
      case SearchDirectionType.West:
        columnNum--;
        break;
      case SearchDirectionType.northWest:
        rowNum++;
        columnNum--;
        break;
      default:
        return undefined;
    }
    if (rowNum > maxRowNum || columnNum > maxColumnNum) {
      // do nothing
    } else {
      const row = this.getRow(rowNum, gameDataModel.gameData);
      if (row) {
        return this.getRow(rowNum, gameDataModel.gameData)[columnNum];
      }
    }
    return undefined;
  }
}
