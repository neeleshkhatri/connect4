import { TestBed, inject } from '@angular/core/testing';

import { GameBoardService } from './game-board.service';
import { GameBoardDimensionsModel } from '../models/game-board-dimensions.model';
import { CellType } from '../models/game-cell.model';

describe('GameBoardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameBoardService]
    });
  });
  it('should have askDimensions set to false', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 4, rows: 4} as GameBoardDimensionsModel);
    expect(service.gameBoard.askDimensions).toBeFalsy();
  }));
  it('should have a board with 4 colums and 4 rows', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 4, rows: 4} as GameBoardDimensionsModel);
    expect(service.gameBoard.gameDataModel.gameData.length).toBe(4);
    expect(service.gameBoard.gameDataModel.gameData[0].length).toBe(4);
  }));
  it('should have a board with 8 colums and 6 rows', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 8, rows: 6} as GameBoardDimensionsModel);
    expect(service.gameBoard.gameDataModel.gameData.length).toBe(6);
    expect(service.gameBoard.gameDataModel.gameData[0].length).toBe(8);
  }));
  it('should set askDimensions to true when resetGame method is called', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.gameBoard.askDimensions = false;
    service.resetGame();
    expect(service.gameBoard.askDimensions).toBe(true);
  }));
  it('Yellow should win horizontally', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 5, rows: 5} as GameBoardDimensionsModel);
    service.dropToken(1); // red
    expect(service.gameBoard.gameDataModel.gameData[4][1].cellType).toBe(CellType.red);
    service.dropToken(1);
    expect(service.gameBoard.gameDataModel.gameData[3][1].cellType).toBe(CellType.yellow);
    service.dropToken(2);
    expect(service.gameBoard.gameDataModel.gameData[4][2].cellType).toBe(CellType.red);
    service.dropToken(2);
    expect(service.gameBoard.gameDataModel.gameData[3][2].cellType).toBe(CellType.yellow);
    service.dropToken(3);
    expect(service.gameBoard.gameDataModel.gameData[4][3].cellType).toBe(CellType.red);
    service.dropToken(3);
    expect(service.gameBoard.gameDataModel.gameData[3][3].cellType).toBe(CellType.yellow);
    service.dropToken(2);
    expect(service.gameBoard.gameDataModel.gameData[2][2].cellType).toBe(CellType.red);
    service.dropToken(4);
    expect(service.gameBoard.gameDataModel.gameData[4][4].cellType).toBe(CellType.yellow);
    service.dropToken(2);
    expect(service.gameBoard.gameDataModel.gameData[1][2].cellType).toBe(CellType.red);
    service.dropToken(4);
    expect(service.gameBoard.gameDataModel.gameData[4][4].cellType).toBe(CellType.yellow);
    expect(service.gameBoard.hasGameFinished).toBe(true);
    expect(service.getCurrentTokenColor()).toBe('yellow');
  }));
  it('Red should win vertically', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 5, rows: 5} as GameBoardDimensionsModel);
    service.dropToken(0); // red
    service.dropToken(1);
    service.dropToken(0); // red
    service.dropToken(1);
    service.dropToken(0); // red
    service.dropToken(1);
    service.dropToken(0); // red
    expect(service.gameBoard.hasGameFinished).toBe(true);
    expect(service.getCurrentTokenColor()).toBe('red');
  }));
  it('Yellow should win diagonally', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 5, rows: 5} as GameBoardDimensionsModel);
    service.dropToken(1); // red
    service.dropToken(0);
    service.dropToken(2);
    service.dropToken(1);
    service.dropToken(2);
    service.dropToken(2);
    service.dropToken(3);
    service.dropToken(3);
    service.dropToken(3);
    service.dropToken(3);
    expect(service.gameBoard.hasGameFinished).toBe(true);
    expect(service.getCurrentTokenColor()).toBe('yellow');
  }));
  it('should find a winner if dropped in between same tokens in horzontal direction',
        inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 5, rows: 5} as GameBoardDimensionsModel);
    service.dropToken(0); // red
    service.dropToken(4);
    service.dropToken(3);
    service.dropToken(4);
    service.dropToken(2);
    service.dropToken(4);
    service.dropToken(1);
    expect(service.gameBoard.hasGameFinished).toBe(true);
    expect(service.getCurrentTokenColor()).toBe('red');
  }));
  it('should find a winner if dropped in between same tokens in diagonally north east direction',
        inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 5, rows: 5} as GameBoardDimensionsModel);
    service.dropToken(0); // red
    service.dropToken(1);
    service.dropToken(2);
    service.dropToken(2);
    service.dropToken(2);
    service.dropToken(3);
    service.dropToken(3);
    service.dropToken(3);
    service.dropToken(3);
    service.dropToken(4);
    service.dropToken(1);
    expect(service.gameBoard.hasGameFinished).toBe(true);
    expect(service.getCurrentTokenColor()).toBe('red');
  }));
  it('should find a winner if dropped in between same tokens in diagonally north west direction',
        inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 5, rows: 5} as GameBoardDimensionsModel);
    service.dropToken(4); // red
    service.dropToken(3);
    service.dropToken(2);
    service.dropToken(2);
    service.dropToken(2);
    service.dropToken(1);
    service.dropToken(1);
    service.dropToken(1);
    service.dropToken(1);
    service.dropToken(0);
    service.dropToken(3);
    expect(service.gameBoard.hasGameFinished).toBe(true);
    expect(service.getCurrentTokenColor()).toBe('red');
  }));
  it('should be a draw', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 4, rows: 4} as GameBoardDimensionsModel);
    service.dropToken(0); // red
    service.dropToken(0);
    service.dropToken(0);
    service.dropToken(0);
    service.dropToken(1);
    service.dropToken(2);
    service.dropToken(3);
    service.dropToken(1);
    service.dropToken(2);
    service.dropToken(3);
    service.dropToken(3);
    service.dropToken(2);
    service.dropToken(1);
    service.dropToken(1);
    service.dropToken(2);
    service.dropToken(3);
    expect(service.gameBoard.hasGameFinished).toBe(true);
    expect(service.gameBoard.isADraw).toBe(true);
  }));
  it('should handle an invalid move', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 4, rows: 4} as GameBoardDimensionsModel);
    service.dropToken(0); // red
    service.dropToken(0); // yellow
    service.dropToken(0); // red
    service.dropToken(0); // yellow
    service.dropToken(0); // This is a invalid move
    service.dropToken(1); // after invalid move
    expect(service.gameBoard.gameDataModel.gameData[3][1].cellType).toBe(CellType.red);
  }));
  it('should handle many invalid moves', inject([GameBoardService], (service: GameBoardService) => {
    expect(service).toBeTruthy();
    service.initalizeGameData({columns: 4, rows: 4} as GameBoardDimensionsModel);
    service.dropToken(0); // red
    service.dropToken(0); // yellow
    service.dropToken(0); // red
    service.dropToken(0); // yellow
    service.dropToken(0); // This is a invalid move
    service.dropToken(0); // This is a invalid move
    service.dropToken(0); // This is a invalid move
    service.dropToken(1); // after invalid moves
    expect(service.gameBoard.gameDataModel.gameData[3][1].cellType).toBe(CellType.red);
  }));
});
