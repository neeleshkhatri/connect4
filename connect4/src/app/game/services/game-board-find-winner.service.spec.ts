import { TestBed, inject } from '@angular/core/testing';

import { GameBoardFindWinnerService } from './game-board-find-winner.service';

describe('GameBoardFindWinnerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameBoardFindWinnerService]
    });
  });

  it('should be created', inject([GameBoardFindWinnerService], (service: GameBoardFindWinnerService) => {
    expect(service).toBeTruthy();
  }));
});
