import { Injectable } from '@angular/core';
import { GameBoardDimensionsModel } from '../models/game-board-dimensions.model';
import { GameCellModel, CellType } from '../models/game-cell.model';
import * as _ from 'underscore';
import { GameBoardFindWinnerService } from './game-board-find-winner.service';
import { GameBoardModel } from '../models/game-board.model';

@Injectable({
  providedIn: 'root'
})
export class GameBoardService {

  gameBoard = new GameBoardModel();

  constructor(private gameBoardFindWinnerService: GameBoardFindWinnerService) { }

  initalizeGameData(dimensions: GameBoardDimensionsModel) {
    this.gameBoard = new GameBoardModel();
    this.gameBoard.askDimensions = false;
    for (let row = dimensions.rows - 1 ; row >= 0; row--) {
    const rowList = new Array<GameCellModel>();
      for (let column = 0; column < dimensions.columns; column++) {
        const cell = new GameCellModel(column, row);
        cell.cellClass = this.getCellClass(CellType.empty);
        rowList.push(cell);
      }
      this.gameBoard.gameDataModel.gameData.push(rowList);
    }
  }

  resetGame() {
    this.gameBoard.askDimensions = true;
  }
  getFirstRow(): Array<GameCellModel> {
    return _.first(this.gameBoard.gameDataModel.gameData);
  }

  dropToken(column: number) {
    const flattenArray:  any = _.flatten(this.gameBoard.gameDataModel.gameData, true);
    const rowsforColumn = _.filter(flattenArray, (cell: any) => {
        return (cell as GameCellModel).column === column;
    });
    const emptyCells = _.filter(rowsforColumn, (cell: any) => {
      return (cell as GameCellModel).cellType === CellType.empty;
    });

    const dropCell = _.last(emptyCells) as GameCellModel;
    if (dropCell) {
      dropCell.cellType = this.gameBoard.tokenColor;
      dropCell.cellClass = this.getCellClass(this.gameBoard.tokenColor);
      if (this.gameBoardFindWinnerService.findWinner(dropCell, this.gameBoard.gameDataModel)) {
        this.gameBoard.hasGameFinished = true;
      } else {
        this.changeTokenColor();
      }
    }

    if (this.checkIfDraw(flattenArray)) {
      this.gameBoard.isADraw = true;
      this.gameBoard.hasGameFinished = true;
    }
  }
  private checkIfDraw(flattenArray): boolean {
    const anyEmptyCells = _.filter(flattenArray, (cell) => (cell as GameCellModel).cellType === CellType.empty);
    if (anyEmptyCells.length === 0) {
      return true;
    }
    return false;
  }
  private changeTokenColor() {
    if (this.gameBoard.tokenColor === CellType.red) {
      this.gameBoard.tokenColor = CellType.yellow;
    } else {
      this.gameBoard.tokenColor = CellType.red;
    }
  }
  private getCellClass(cellType: CellType): string {
    if (cellType === CellType.red) {
      return 'red';
    } else if (cellType === CellType.yellow) {
      return 'yellow';
    } else {
      return 'white';
    }
  }
  public getCurrentTokenColor(): string {
    return CellType[this.gameBoard.tokenColor];
  }
}
