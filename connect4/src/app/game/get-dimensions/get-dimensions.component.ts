import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { GameBoardService } from '../services/game-board.service';
import { GameBoardDimensionsModel } from '../models/game-board-dimensions.model';

@Component({
  selector: 'app-get-dimensions',
  templateUrl: './get-dimensions.component.html',
  styleUrls: ['./get-dimensions.component.scss']
})
export class GetDimensionsComponent implements OnInit {

  dimensionsForm = this.fb.group({
    columns: ['', [Validators.required, Validators.min(4), Validators.max(9)]],
    rows: ['', [Validators.required, Validators.min(4), Validators.max(9)]]
  });
  constructor(private fb: FormBuilder, private gameBoardService: GameBoardService) { }

  ngOnInit() {
  }

  onSubmit() {
    const dimensions: {columns: number, rows: number} = this.dimensionsForm.value as {columns: number, rows: number};
    this.gameBoardService.initalizeGameData({columns: dimensions.columns, rows: dimensions.rows} as GameBoardDimensionsModel );
  }
}
