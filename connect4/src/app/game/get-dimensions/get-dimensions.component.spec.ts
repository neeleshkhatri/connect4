import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { GetDimensionsComponent } from './get-dimensions.component';
import { MockComponent } from 'ng2-mock-component';
import { FormBuilder } from '@angular/forms';

describe('GetDimensionsComponent', () => {
  let component: GetDimensionsComponent;
  let fixture: ComponentFixture<GetDimensionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetDimensionsComponent
        , MockComponent({ selector: 'form', inputs: ['formGroup'] })
        , MockComponent({ selector: 'app-field-error', inputs: ['field'] })
        ],
    providers: [
        FormBuilder
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetDimensionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  function updateForm(columns: number, rows: number) {
    component.dimensionsForm.controls['columns'].setValue(columns);
    component.dimensionsForm.controls['rows'].setValue(rows);
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('fields are required', fakeAsync(() => {
    expect(component.dimensionsForm.controls['columns'].errors.required).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].errors.required).toBeTruthy();
    expect(component.dimensionsForm.controls['columns'].valid).toBeFalsy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeFalsy();
  }));
  it('fields are invalid when less than 4', fakeAsync(() => {
    updateForm(3, 3);
    expect(component.dimensionsForm.controls['columns'].errors.min).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].errors.min).toBeTruthy();
    expect(component.dimensionsForm.controls['columns'].valid).toBeFalsy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeFalsy();

    updateForm(1, 2);
    expect(component.dimensionsForm.controls['columns'].errors.min).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].errors.min).toBeTruthy();
    expect(component.dimensionsForm.controls['columns'].valid).toBeFalsy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeFalsy();

    updateForm(-1, 0);
    expect(component.dimensionsForm.controls['columns'].errors.min).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].errors.min).toBeTruthy();
    expect(component.dimensionsForm.controls['columns'].valid).toBeFalsy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeFalsy();
  }));
  it('fields are invalid when greater than 9', fakeAsync(() => {
    updateForm(10, 10);
    expect(component.dimensionsForm.controls['columns'].errors.max).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].errors.max).toBeTruthy();
    expect(component.dimensionsForm.controls['columns'].valid).toBeFalsy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeFalsy();

    updateForm(20, 11);
    expect(component.dimensionsForm.controls['columns'].errors.max).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].errors.max).toBeTruthy();
    expect(component.dimensionsForm.controls['columns'].valid).toBeFalsy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeFalsy();
  }));
  it('fields are valid when value are between >=4 and =<9', fakeAsync(() => {
    updateForm(9, 9);
    expect(component.dimensionsForm.controls['columns'].valid).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeTruthy();

    updateForm(4, 4);
    expect(component.dimensionsForm.controls['columns'].valid).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeTruthy();

    updateForm(5, 6);
    expect(component.dimensionsForm.controls['columns'].valid).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeTruthy();

    updateForm(8, 8);
    expect(component.dimensionsForm.controls['columns'].valid).toBeTruthy();
    expect(component.dimensionsForm.controls['rows'].valid).toBeTruthy();
  }));
});
