import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameCellComponent } from './game-cell.component';
import { GameCellModel } from '../models/game-cell.model';

describe('GameCellComponent', () => {
  let component: GameCellComponent;
  let fixture: ComponentFixture<GameCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameCellComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameCellComponent);
    component = fixture.componentInstance;
    component.cell = new GameCellModel(0, 0);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
