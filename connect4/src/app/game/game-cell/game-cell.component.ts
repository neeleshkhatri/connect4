import { Component, OnInit, Input } from '@angular/core';
import { GameCellModel } from '../models/game-cell.model';

@Component({
  selector: 'app-game-cell',
  templateUrl: './game-cell.component.html',
  styleUrls: ['./game-cell.component.scss']
})
export class GameCellComponent implements OnInit {

  @Input() cell: GameCellModel;
  constructor() { }

  ngOnInit() {
  }

}
