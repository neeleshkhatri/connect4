# Connect4

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.7.

## Environment Setup Steps
1. Install Node if not already installed. 
        Install from [`https://nodejs.org/en/download/`](https://nodejs.org/en/download/)

2. Install Angular CLI if not already installed
Follow steps from [`https://github.com/angular/angular-cli/wiki`](https://github.com/angular/angular-cli/wiki)

3. Run Npm Install
Run `npm install` from the connect4 folder which has the package.json file.
4. Start the Angular app
Run `ng serve` from the connect4 folder which has the package.json file.
Then navigate to [`http://localhost:4200/`](http://localhost:4200/) in your browser.

## Demo Link
Demo link is [`https://neeleshkhatri.bitbucket.io/connect4/`](https://neeleshkhatri.bitbucket.io/connect4/)
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running unit tests with code coverage report
Run `ng test --code-coverage` 

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
